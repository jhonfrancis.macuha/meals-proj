import { Component, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { IonSlides } from '@ionic/angular';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {

  @ViewChild(IonSlides) slides: IonSlides;

  constructor(public router: Router) { }






  ngOnInit() {
  }

  // next(){
  //   this.slides.slideNext();
  // }

  skip(){
    this.gotoHome();
  }

  gotoHome(){
    this.router.navigate(['homepage']);
  }



}


