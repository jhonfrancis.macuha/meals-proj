import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController, ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-myrecipes',
  templateUrl: './myrecipes.page.html',
  styleUrls: ['./myrecipes.page.scss'],
})
export class MyrecipesPage implements OnInit {


  saved:any;

  constructor(public router: NavController, public api_service: ApiService,public toastController:ToastController) { 


    this.getSavedRecipes();
  }

  ngOnInit() {
  }


  home(){
    this.router.navigateBack("/homepage");
  }


  getSavedRecipes(){
    this.api_service.getSavedRecipes().subscribe((res:any)=> {
      console.log("Success===",res);
      this.saved=res;

     
    },(error)=> {
      console.log("Error===",error);
    })

  }

  async delete(id){
    this.api_service.deleteSavedRecipes(id).subscribe((res:any)=> {
      console.log("Success===",res);

      

      this.getSavedRecipes();
      

     
    },(error)=> {
      console.log("Error===",error);
    })
    const toast = await this.toastController.create({
      message: 'Recipe deleted',
      duration: 2000
    });
    toast.present();
  }

  

  

}
