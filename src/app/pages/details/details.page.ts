import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ActivatedRoute } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  id:any;


  recipe:any;

  ingredients:any;


  constructor(public toastController: ToastController,public route: ActivatedRoute ,public api_service:ApiService,public dom:DomSanitizer) {

    this.id=this.route.snapshot.paramMap.get('id');

    console.log(this.id)

    this.getRecipe();

    this.getIngr();
   }

  ngOnInit() {
  }

  getRecipe(){

    this.api_service.getSingleRecipe(this.id).subscribe((res:any)=> {
      console.log("Success===",res);
      this.recipe=res;

     
    },(error)=> {
      console.log("Error===",error);
    })

    



  }

  getIngr(){
    this.api_service.getIngr(this.id).subscribe((res:any)=> {
      console.log("Success===",res);

      this.ingredients=res
      

     
    },(error)=> {
      console.log("Error===",error);
    })

  }

  sanitize(vid){
    return this.dom.bypassSecurityTrustResourceUrl(vid);
  }
  async presentToast() {
    
  }


  async save(id){


    
    this.recipe.forEach(c => {
      let data={
        recipe_id:c.id,
        name:c.name,
        image:c.image

        
      }
      this.api_service.saveRecipe(data).subscribe((res:any)=> {
        console.log("Success===",res);
        
  
       
      },(error)=> {
        console.log("Error===",error);
      })
      
      
    });
    const toast = await this.toastController.create({
      message: 'Recipe Saved',
      duration: 2000
    });
    toast.present();
    

    


  }




}
