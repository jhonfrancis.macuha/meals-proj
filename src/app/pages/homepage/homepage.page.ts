import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.page.html',
  styleUrls: ['./homepage.page.scss'],
})
export class HomepagePage implements OnInit {

  recipes:any;


  searches:any;

  constructor(public api_service: ApiService, public router: Router) {

    
    this.getRecipe();
    

    

    
   }

  ngOnInit() {
  }

  getRecipe(){

    this.api_service.getRecipes().subscribe((res:any)=> {
      console.log("Success===",res);
      this.recipes=res;

     
    },(error)=> {
      console.log("Error===",error);
    })

    



  }
  





  search(){

    // console.log(this.searches)
    this.api_service.searchRecipes(this.searches).subscribe((res:any)=> {
      console.log("Success===",res);
      this.recipes=res;

     
    },(error)=> {
      console.log("Error===",error);
    })

    



  }


  details(id){

    this.router.navigate(['details',id])

  }

}
