import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ApiService {


  api_key="c1aad68027afeed6acd0446ed0caa1c2	";
  app_id="2835b679";

  headers:HttpHeaders;

  constructor(public http: HttpClient) { 

    this.headers =new HttpHeaders();
    this.headers.append("Accept", 'application/json');
    this.headers.append("Content-Type", 'application/json');
    this.headers.append("Access-Control-Allow-Origin", '*');
  }


  // getRecipe(){
  //   return this.http.get('https://api.edamam.com/search?app_id=$2835b679&app_key=$c1aad68027afeed6acd0446ed0caa1c2&q=chicken');

  // }

  getRecipes(){
    return this.http.get('http://localhost/Projects/Meals2go/server/getAllRecipes.php');

  }

  searchRecipes(name){
    return this.http.get('http://localhost/Projects/Meals2go/server/searchRecipes.php?name='+name);

  }

  getSingleRecipe(id){
    return this.http.get('http://localhost/Projects/Meals2go/server/getSingleRecipe.php?id='+id);


  }

  getIngr(id){
    return this.http.get('http://localhost/Projects/Meals2go/server/getIngredients.php?id='+id);

  }


  saveRecipe(data){
    return this.http.post('http://localhost/Projects/Meals2go/server/create.php',data);



  }



  getSavedRecipes(){
    return this.http.get('http://localhost/Projects/Meals2go/server/getSavedRecipes.php');

  }

  deleteSavedRecipes(id){
    return this.http.delete('http://localhost/Projects/Meals2go/server/deleteSavedRecipes.php?id='+id);

  }
}
